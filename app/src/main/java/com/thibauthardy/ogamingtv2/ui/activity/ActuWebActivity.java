package com.thibauthardy.ogamingtv2.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import com.thibauthardy.ogamingtv2.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 09/12/2014.
 */
public class ActuWebActivity extends Activity {

    @InjectView(R.id.wv_actu)
    WebView mWebView;
    @InjectView(R.id.tv_actu_web)
    TextView mTextView;
    String url;
    FloatingActionButton centerLowerButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actu_web);
        ButterKnife.inject(this);
        Intent intent = getIntent();


        //TODO test connexion
        if(getIntent() != null){
            mTextView.setVisibility(View.GONE);
            url = intent.getStringExtra("URL");
            mWebView.loadUrl(url);
        }else{
            mWebView.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText("Pas le Lien");
        }
        initFloatingButton();
    }

    public void initFloatingButton(){
        ImageView fabIconRight = new ImageView(this);
        fabIconRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_new_light));
        centerLowerButton = new FloatingActionButton.Builder(this)
                .setContentView(fabIconRight)
                .setBackgroundDrawable(R.drawable.fab_selector_blue)
                .setPosition(FloatingActionButton.POSITION_BOTTOM_CENTER)
                .build();

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(this);
        ImageView rlIcon1 = new ImageView(this);
        ImageView rlIcon2 = new ImageView(this);
        ImageView rlIcon3 = new ImageView(this);
        ImageView rlIcon4 = new ImageView(this);

        rlIcon1.setImageDrawable(getResources().getDrawable(R.drawable.ic_facebook));
        rlIcon2.setImageDrawable(getResources().getDrawable(R.drawable.ic_googleplus));
        rlIcon3.setImageDrawable(getResources().getDrawable(R.drawable.ic_twitter));
        rlIcon4.setImageDrawable(getResources().getDrawable(R.drawable.ic_instagram));

        // Build the menu with default options: light theme, 90 degrees, 72dp radius.
        // Set 4 default SubActionButtons
        FloatingActionMenu myLowerMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(rLSubBuilder.setContentView(rlIcon1).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon2).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon3).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon4).build())
                .setStartAngle(0)
                .setEndAngle(-180)
                .attachTo(centerLowerButton)
                .build();

        rlIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "clic on sub item 1", Toast.LENGTH_SHORT).show();
            }
        });
        rlIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"clic on sub item 2",Toast.LENGTH_SHORT).show();
            }
        });
        rlIcon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        rlIcon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"clic on sub item 4",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
