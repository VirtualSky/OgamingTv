package com.thibauthardy.ogamingtv2.ui.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.utils.PreferenceManager;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by Thibault HARDY on 24/11/2014.
 */
public class SplashscreenLogoActivity extends Activity {

    @InjectView(R.id.cb_splashcreeenLogo)
    CheckBox mCheckBox;

    @InjectView(R.id.bt_notnow)
    Button mButtonNotNow;
    @InjectView(R.id.bt_splaashscreen_login)
    Button mButtonLogin;

    @InjectView(R.id.et_splashscreen_speudo)
    EditText mEditTextSpeudo;

    @InjectView(R.id.et_splashscreen_mdp)
    EditText mEditTextMdp;


    private Context mContext;
    public static final String HIDE_VIDEO_SPLASHSCREEN = "HIDE_VIDEO_SPLASHSCREEN" ;
    PreferenceManager mPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen_logo_card_new_style);
        ButterKnife.inject(this);
        mContext = this;
        mPreferenceManager = new PreferenceManager(SplashscreenLogoActivity.this);
        mCheckBox.setChecked(mPreferenceManager.getDataBoolean(HIDE_VIDEO_SPLASHSCREEN));

        mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPreferenceManager.putDataBoolean(HIDE_VIDEO_SPLASHSCREEN, mCheckBox.isChecked());
            }
        });

        mButtonNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchActivity();
            }
        });
    }

    public void launchActivity(){
        Intent intent = new Intent(SplashscreenLogoActivity.this, HomePageActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.flip_out, R.anim.flip_in);
    }
}
