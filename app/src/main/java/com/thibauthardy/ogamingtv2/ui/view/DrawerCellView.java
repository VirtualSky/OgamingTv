package com.thibauthardy.ogamingtv2.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.DrawerItem;
import com.thibauthardy.ogamingtv2.tools.genelist.AbstractCellView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 02/12/2014.
 */
public class DrawerCellView extends AbstractCellView<DrawerItem> {


    @InjectView(R.id.iv_separator)
    ImageView mImageSeparator;
    @InjectView(R.id.ll_drawer_list_item)
    LinearLayout mLinearLayout;
    @InjectView(R.id.iv_drawer_image_item)
    ImageView mImage = null;
    @InjectView(R.id.tv_drawer_list_item)
    TextView mTvTitle = null;
    @InjectView(R.id.iv_drawer_image_live_item)
    ImageView mLiveImage = null;


    public DrawerCellView(Context context){
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.drawer_list_view_item, this);
        ButterKnife.inject(this,view);
    }

    @Override
    public void setData(DrawerItem drawerItem) {
        mImage.setImageResource(drawerItem.getImage());
        mTvTitle.setText(drawerItem.getTitle());

        if(drawerItem.isLive()){
            mLiveImage.setVisibility(VISIBLE);
            mLiveImage.setImageResource(drawerItem.getImageLive());
        }else{
            mLiveImage.setVisibility(GONE);
        }
        if(drawerItem.isSeparator()){
            mImageSeparator.setVisibility(VISIBLE);
            mLinearLayout.setVisibility(GONE);
        }else{
            mImageSeparator.setVisibility(GONE);
            mLinearLayout.setVisibility(VISIBLE);
        }
        if(drawerItem.isFooter()){
            mLinearLayout.setBackgroundColor(getResources().getColor(R.color.OGMarronEndPage));
            mTvTitle.setTextColor(getResources().getColor(R.color.OGBeigeEndPolice));
        }
    }
}
