package com.thibauthardy.ogamingtv2.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardLiveItem;
import com.thibauthardy.ogamingtv2.models.adapter.CardLiveAdapter;
import com.thibauthardy.ogamingtv2.ui.activity.LiveWebActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class LiveFragment extends Fragment {

    @InjectView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;

    Bundle args;
    String mTri;
    private LinearLayoutManager mLayoutManager;
    private CardLiveAdapter mAdapter;
    private ArrayList<CardLiveItem> myDataset;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getArgumentFromActivity();
        initDataSet();

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new CardLiveAdapter(myDataset,getActivity().getPackageName());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new CardLiveAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                CardLiveItem mCardLiveItem = mAdapter.getItem(position);
                Intent intent = new Intent(getActivity(),LiveWebActivity.class);
                intent.putExtra("URL_TWITCH", mCardLiveItem.getLinkTwitch());
                intent.putExtra("URL_CHAT",mCardLiveItem.getUrlChat());
                Toast.makeText(getActivity(),mCardLiveItem.getUrlVideo(),Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

    }

    private void initDataSet() {
        myDataset = new ArrayList<CardLiveItem>();
        CardLiveItem myCardLiveItem;
        myCardLiveItem = new CardLiveItem(R.drawable.live_lol_generaliste,"http://www-cdn.jtvnw.net/swflibs/TwitchPlayer.refe9d3fbc78d7dcdc1bd603943f6909be23b7a07.swf?old=1&channel=OgamingLoL",R.raw.lcs);
        myDataset.add(myCardLiveItem);
        myCardLiveItem = new CardLiveItem(R.drawable.live_punchline,"http://www.twitch.tv/widgets/live_embed_player.swf?channel=OgamingHS",R.raw.hs);
        myDataset.add(myCardLiveItem);
        myCardLiveItem = new CardLiveItem(R.drawable.live_hearthstone_generaliste,"http://www.twitch.tv/widgets/live_embed_player.swf?channel=OgamingHS",R.raw.hs);
        myDataset.add(myCardLiveItem);
        myCardLiveItem = new CardLiveItem(R.drawable.live_sc2_generaliste,"http://www.twitch.tv/widgets/live_embed_player.swf?channel=OgamingHS",R.raw.sc2);
        myDataset.add(myCardLiveItem);
        myCardLiveItem = new CardLiveItem(R.drawable.live_ogaming_generaliste,"http://www.twitch.tv/widgets/live_embed_player.swf?channel=OgamingHS",R.raw.heroes);
        myDataset.add(myCardLiveItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycleview, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    public void getArgumentFromActivity() {
        args = getArguments();
        if (args != null) {
            mTri = args.getString("ACTU");
        } else {
            mTri = "lol";
        }
    }
}
