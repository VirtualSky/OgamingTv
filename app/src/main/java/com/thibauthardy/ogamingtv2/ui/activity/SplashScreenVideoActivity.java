package com.thibauthardy.ogamingtv2.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.utils.PreferenceManager;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class SplashScreenVideoActivity extends Activity {

    /**
     * Whether or not we're showing the back of the card (otherwise showing the front).
     */

    @InjectView(R.id.vv_splashScreen)
    VideoView mVideoView;
    private Intent mIntent;
    PreferenceManager mPreferenceManager;
    public static final String HIDE_VIDEO_SPLASHSCREEN = "HIDE_VIDEO_SPLASHSCREEN" ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen_video_card);
        ButterKnife.inject(this);
       mPreferenceManager = new PreferenceManager(this);
        mIntent = new Intent(this, SplashscreenLogoActivity.class);
        if (mPreferenceManager.getDataBoolean(HIDE_VIDEO_SPLASHSCREEN) == true) {
            changeToLogo(false);
        } else {
            player();
        }
              mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        changeToLogo(true);
                    }
            });
            mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    //TODO creer une dialogue de crash
                    Log.e("media player","video failed");

                    return false;
                }
            });
    }
    public void changeToLogo(Boolean animation){

        Log.e("anim","je change");
        startActivity(mIntent);
        finish();
       if(animation){
           this.overridePendingTransition(R.anim.flip_out, R.anim.flip_in);
       }

}

    public void player(){
        Log.i("player()", "i play now");
        mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.intro_ogamingtv));
        mVideoView.start();
    }
}

