package com.thibauthardy.ogamingtv2.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.DrawerItem;
import com.thibauthardy.ogamingtv2.tools.genelist.ThunderbotAdapter;
import com.thibauthardy.ogamingtv2.ui.fragments.ActuFragment;
import com.thibauthardy.ogamingtv2.ui.fragments.LastVodsFragment;
import com.thibauthardy.ogamingtv2.ui.fragments.LiveFragment;
import com.thibauthardy.ogamingtv2.ui.view.DrawerCellView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 02/12/2014.
 */
public class HomePageActivity  extends ActionBarActivity {

    @InjectView(R.id.left_drawer)
    ListView mDrawerList;

    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    private final static String TAG = "HOMEPAGE_ACTIVITY";

    private FragmentManager mFragmentManager;
    private ThunderbotAdapter mAdapter;
    private ArrayList<DrawerItem> mData;
    private Bundle arguments;

    private ActionBarDrawerToggle mDrawerToggle;
    private Context mContext;
    private final static String URLSHOP = "http://stores.ebay.fr/ogamingstore";
    private final static String URLTWITTER = "https://twitter.com/OGaming_TV";
    private final static String URLFACEBOOK = "https://www.facebook.com/OGamingTV";



    //TODO faire une fichier avec toutes les grosses constante de l'appli

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        ButterKnife.inject(this);
        mContext = this;
        initData();
        mAdapter = new ThunderbotAdapter(this,mData, DrawerCellView.class);
        mDrawerList.setAdapter(mAdapter);
        if (mToolbar != null) {
            mToolbar.setTitleTextColor(getResources().getColor(R.color.OGOrange));
            setSupportActionBar(mToolbar);
        }
        initDrawer();
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DrawerItem item = (DrawerItem)mAdapter.getItem(position);
                if(item.getActivity() == null){
                    mFragmentManager.beginTransaction().replace(R.id.content_frame, item.getFragment()).commit();
                    mDrawerLayout.closeDrawer(mDrawerList);
                }else{
                    Intent intent = new Intent(mContext,item.getActivity());
                    if(item.getTitle().equalsIgnoreCase("FaceBook")){
                        intent.putExtra("URL",URLFACEBOOK);
                    }
                    if(item.getTitle().equalsIgnoreCase("Twitter")){
                        intent.putExtra("URL",URLTWITTER);
                    }
                    if(item.getTitle().equalsIgnoreCase("Shop")){
                        intent.putExtra("URL",URLSHOP);
                    }
                    mDrawerLayout.closeDrawer(mDrawerList);
                    startActivity(intent);
                }

            }
        });

    }

    private void initData() {
        mData = new ArrayList<DrawerItem>();
        DrawerItem drawerItem = new DrawerItem(R.drawable.ic_logo_ogaming,"Actu",R.drawable.ic_logo_ogaming,false,new ActuFragment(),false,false);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(R.drawable.ic_logo_ogaming,"Live",R.drawable.ic_logo_ogaming,true,new LiveFragment(),false,false);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(R.drawable.ic_logo_ogaming,"Vods",R.drawable.ic_logo_ogaming,false,new LastVodsFragment(),false,false);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(R.drawable.ic_logo_ogaming,"Agenda",R.drawable.ic_logo_ogaming,false,new ActuFragment(),false,false);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(R.drawable.ic_logo_ogaming,"Shop",R.drawable.ic_logo_ogaming,false,ActuWebActivity.class,false,false);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(0,"",0,false,new ActuFragment(),true,true);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(android.R.drawable.ic_menu_preferences,"Preference",0,false,new ActuFragment(),true,false);
        //TODO créer la page des preferences
        mData.add(drawerItem);
        drawerItem = new DrawerItem(R.drawable.ic_twitter,"Twitter",0,false,ActuWebActivity.class,true,false);
        mData.add(drawerItem);
        drawerItem = new DrawerItem(R.drawable.ic_facebook,"FaceBook",0,false,ActuWebActivity.class,true,false);
        mData.add(drawerItem);
    }

    private void initDrawer(){
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        try{
            final ActionBarActivity activity = (ActionBarActivity) mContext;
            // Return the fragment manager
            mFragmentManager = activity.getSupportFragmentManager();
            mFragmentManager.beginTransaction().replace(R.id.content_frame, new ActuFragment()).commit();

        } catch (ClassCastException e) {
            Log.d(TAG, "Can't get the fragment manager with this");
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

//TODO replace buy diologue fo rconnexion
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_drawer) {
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
