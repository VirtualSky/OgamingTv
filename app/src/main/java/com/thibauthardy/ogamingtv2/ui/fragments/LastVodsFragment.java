package com.thibauthardy.ogamingtv2.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardLastVodItem;
import com.thibauthardy.ogamingtv2.models.adapter.CardLastVodAdapter;
import com.thibauthardy.ogamingtv2.models.adapter.CardLastVodAdapter.OnItemClickListener;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;



/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class LastVodsFragment extends Fragment {

    @InjectView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;

    Bundle args;
    String mTri;
    private LinearLayoutManager mLayoutManager;
    private CardLastVodAdapter mAdapter;
    private ArrayList<CardLastVodItem> myDataset;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getArgumentFromActivity();
        initDataSet();

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new CardLastVodAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(View view, int position) {
                CardLastVodItem mCardLastVodItem = mAdapter.getItem(position);
                Toast.makeText(getActivity(), mCardLastVodItem.getTitle(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private  void initDataSet(){
        myDataset = new ArrayList<CardLastVodItem>();
        CardLastVodItem myCardLastVodItem = new CardLastVodItem("10 DEC 2014", " - 0  ","LEAGUE OF LEGENDS",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("11 DEC 2014"," - 1  ","DIVERS",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("12 DEC 2014"," - 2  ","HEARTSTONE",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("13 DEC 2014"," - 3  ","STRACRAFT",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("14 DEC 2014"," - 4  ","LEAGUE OF LEGENDS",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("15 DEC 2014"," - 5  ","FPS",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("16 DEC 2014"," - 6  ","DIVERS",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR]");
        myDataset.add(myCardLastVodItem);
        myCardLastVodItem = new CardLastVodItem("17 DEC 2014"," - 7  ","LEAGUES OF LEGENDS",R.drawable.vod_image_test,"(HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR] (HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM (HD976) TAEJA VS LIFE - TVZ - HEART OF THE SWARM [FR] ");
        myDataset.add(myCardLastVodItem);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycleview, container, false);
        ButterKnife.inject(this, view);
        return view;
    }
    public void getArgumentFromActivity() {
        args = getArguments();
        if (args != null) {
            mTri = args.getString("ACTU");
        } else {
            mTri = "lol";
        }
    }
}
