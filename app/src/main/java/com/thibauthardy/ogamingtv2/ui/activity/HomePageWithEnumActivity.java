package com.thibauthardy.ogamingtv2.ui.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.MenuExpandableAdapter;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuItem;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuNewItem;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuTvItem;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuVideoItem;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerSubMenuItem;
import com.thibauthardy.ogamingtv2.ui.fragments.ActuFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 25/11/2014.
 */
public class HomePageWithEnumActivity extends ActionBarActivity {

    @InjectView(R.id.left_drawer)
    ExpandableListView mDrawerList;

    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    private Fragment mNextFragment;
    private FragmentManager mFragmentManager;
    private MenuExpandableAdapter mMenuExpandableAdapter;
    private Bundle arguments;

    private ActionBarDrawerToggle mDrawerToggle;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage_expandable);
        ButterKnife.inject(this);

        mMenuExpandableAdapter = new MenuExpandableAdapter(this);
        mDrawerList.setAdapter(mMenuExpandableAdapter);
        if (mToolbar != null) {
            mToolbar.setTitleTextColor(getResources().getColor(R.color.OGOrange));
            setSupportActionBar(mToolbar);
        }
        initDrawer();

        //gestion des actions du drawer
        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                DrawerSubMenuItem subMenuItem = mMenuExpandableAdapter.getChild(groupPosition, childPosition);
                Log.i("onchildclic",subMenuItem.toString());
                if(subMenuItem instanceof DrawerMenuNewItem){
                    Log.i("onchildclic","new");
                    DrawerMenuNewItem menu   = (DrawerMenuNewItem) subMenuItem;
                    switch (menu) {
                        case All:
                            arguments.putString("ACTU","all");
                            break;
                        case LOL :
                            arguments.putString("ACTU","lol");
                            break;
                        case STARCRAFT:
                            arguments.putString("ACTU","starcraft");
                            break;

                        case DOC:
                            arguments.putString("ACTU","doc");
                            break;

                        case HEARTHSTONE:
                            arguments.putString("ACTU","hearthstone");
                            break;

                        case HOTS :
                            arguments.putString("ACTU","hots");
                            break;

                        case DOTA :
                            arguments.putString("ACTU","dota");
                            break;

                        case FPS :
                            arguments.putString("ACTU","fps");
                            break;

                        case VF :
                            arguments.putString("ACTU","vf");
                            break;

                        case DIVERS :
                            arguments.putString("ACTU","divers");
                            break;

                        default:
                            break;
                    }
                    mNextFragment = new ActuFragment();
                }else if( subMenuItem instanceof DrawerMenuTvItem){
                    Log.i("onchildclic","tv");
                    DrawerMenuTvItem menu  = (DrawerMenuTvItem) subMenuItem;
                    switch (menu) {
                        case LOL:
                            arguments.putString("TV","lol");
                            break;
                        case STARCRAFT:
                            arguments.putString("TV","starcraft");
                            break;
                        case HEARTHSTONE:
                            arguments.putString("TV","hearthstone");
                            break;
                        case ANNOS:
                            arguments.putString("TV","annos");
                            break;
                        case LORGARD:
                            arguments.putString("TV","lorgard");
                            break;
                        case SILVERMEDUSA:
                            arguments.putString("TV","silvermedusa");
                            break;
                        case GENERALIST:
                            arguments.putString("TV","generalist");
                            break;
                        default:
                            break;
                    }
                    mNextFragment = new ActuFragment();
                }else{
                    Log.i("onchildclic","video");
                    DrawerMenuVideoItem menu  = (DrawerMenuVideoItem) subMenuItem;
                    switch (menu) {
                        case OGAMING:
                            arguments.putString("Video","ogaming");
                            break;
                        case CHIPSNOI:
                            arguments.putString("Video","chipsnoi");
                            break;
                        case PONFTHUD:
                            arguments.putString("Video","ponfthud");
                            break;
                        default:
                            break;
                    }
                    mNextFragment = new ActuFragment();
                }
                //TODO lance le fragment avec les parametres qu'ils faut
                launchFragment(mNextFragment, arguments);
                mDrawerToggle.onDrawerClosed(mDrawerLayout);
                return false;

            }
        });

        mDrawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Log.i("ITEMClick","jai appuyer su "+groupPosition);

                DrawerMenuItem item = mMenuExpandableAdapter.getGroup(groupPosition);
                Log.i("ongroupChild",item.toString());
                switch (item) {
                    case MENU_ACTU:
                        break;
                    case MENU_WEBTV:
                        break;
                    case MENU_AGENDA:
                        mToolbar.setTitle("Actu");
                        mDrawerLayout.closeDrawers();
                        mDrawerList.collapseGroup(groupPosition);
                        mNextFragment = new ActuFragment();
                        break;
                    case MENU_VIDEO:
                        break;
                    case MENU_SHOP:
                        mToolbar.setTitle("Shop");
                        mDrawerLayout.closeDrawers();
                        mDrawerList.collapseGroup(groupPosition);
                        mNextFragment = new ActuFragment();
                        break;
                    case MENU_FORUM:
                        mToolbar.setTitle("Forum");
                        mDrawerLayout.closeDrawers();
                        mDrawerList.collapseGroup(groupPosition);
                        mNextFragment = new ActuFragment();
                        break;
                    default:
                        break;
                }
                launchFragment(mNextFragment,arguments);
                return false;
            }
        });
    }

    //creation  de la navigation  du drawer
    private void initDrawer(){
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                    collapseAll();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mFragmentManager = getSupportFragmentManager();
        launchFragment(new ActuFragment(),null);
    }

    private void launchFragment(Fragment fragment,Bundle arguments){
        // Insert the fragment by replacing any existing fragment
        if(fragment == null) mNextFragment = new ActuFragment();
        mNextFragment = fragment;
        if(arguments!=null){
            mNextFragment.setArguments(arguments);
        }
        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.content_frame, mNextFragment).commit();
    }
    private void collapseAll(){
        for(int i = 0; i <=mMenuExpandableAdapter.getGroupCount();i++ ){
            mDrawerList.collapseGroup(i);
        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_drawer) {
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
