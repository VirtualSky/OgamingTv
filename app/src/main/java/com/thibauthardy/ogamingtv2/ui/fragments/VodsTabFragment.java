package com.thibauthardy.ogamingtv2.ui.fragments;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.models.adapter.TabPageAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 02/12/2014.
 */
public class VodsTabFragment extends Fragment implements ActionBar.TabListener{

    @InjectView(R.id.vods_pager)
    ViewPager mViewPager;

    private TabPageAdapter mAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_vods, container, false);
        ButterKnife.inject(this, view);
        mAdapter = new TabPageAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {}

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {}

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {}
}
