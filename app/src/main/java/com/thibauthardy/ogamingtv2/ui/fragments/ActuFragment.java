package com.thibauthardy.ogamingtv2.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardActuItem;
import com.thibauthardy.ogamingtv2.models.adapter.CardActuAdapter;
import com.thibauthardy.ogamingtv2.ui.activity.ActuWebActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 27/11/2014.
 */
public class ActuFragment extends Fragment {

    @InjectView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;


    Bundle args;
    String mTri;
    private LinearLayoutManager mLayoutManager;
    private CardActuAdapter mAdapter;
    private ArrayList<CardActuItem> myDataset;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getArgumentFromActivity();
        initDataSet();

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new CardActuAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new CardActuAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                CardActuItem mCardActuItem = mAdapter.getItem(position);
                Toast.makeText(getActivity(),mCardActuItem.getTitle(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(),ActuWebActivity.class);
                intent.putExtra("URL", mCardActuItem.getLink());
                startActivity(intent);
            }
        });
    }

    private void initDataSet() {
        //TODO   voir comment gerer les actus
        //recuperation dans un service du flux rss
        // recuperation d'une liste en connecter ou stockage en base.
        // et recup depuis la base de données
        // idem pou rles vods
        myDataset = new ArrayList<CardActuItem>();
        CardActuItem myCardActuItem;
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"http://www.kikou.com");

        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"http://www.kikou.com");
        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"http://www.kikou.com");
        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"http://www.kikou.com");
        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"http://www.kikou.com");
        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"http://www.kikou.com");
        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"");
        myDataset.add(myCardActuItem);
        myCardActuItem = new CardActuItem(R.drawable.card_actu_test,"10 DEC. 2014","- SHADROOD ","- 06","LEAGUES OF LEGENDS",getString(R.string.actu_title_test),getString(R.string.actu_contents_test),"");
        myDataset.add(myCardActuItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycleview, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    public void getArgumentFromActivity() {
        args = getArguments();
        if (args != null) {
            mTri = args.getString("ACTU");
        } else {
            mTri = "lol";
        }
    }
}
