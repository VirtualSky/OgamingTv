package com.thibauthardy.ogamingtv2.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;

/**
 * Created by Thibault HARDY on 27/11/2014.
 */
public class CustomFontTextView extends TextView {


    public CustomFontTextView(Context context) {
        super(context);

    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //recupere un tableau d'attribut
        TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.customTextView);
        initFont(a.getString(R.styleable.customTextView_customFontPath));
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void initFont(String font){
        if(font != null ){
            Typeface tf = Typeface.createFromAsset(getContext()
                    .getAssets(), font);
            if (tf !=null) {
                setTypeface(tf);
            }
        }
    }
}
