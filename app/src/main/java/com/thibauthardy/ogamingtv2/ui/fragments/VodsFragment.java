package com.thibauthardy.ogamingtv2.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardPlayListItem;
import com.thibauthardy.ogamingtv2.models.adapter.CardPlayListAdapter;
import com.thibauthardy.ogamingtv2.models.adapter.CustomSpinnerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Thibault HARDY on 02/12/2014.
 */
public class VodsFragment extends Fragment{

    @InjectView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;

    @InjectView(R.id.sp_type)
    Spinner mTypeSpinner;
    @InjectView(R.id.sp_bonus)
    Spinner mBonusSpinner;
    @InjectView(R.id.sp_competition)
    Spinner mCompetitionSpinner;
    @InjectView(R.id.sp_emission)
    Spinner mEmissionSpinner;
    @InjectView(R.id.sp_jeu)
    Spinner mJeuSpinner;
    @InjectView(R.id.sp_commentateur)
    Spinner mCommentateurSpinner;


    Bundle args;
    String mTri;
    private LinearLayoutManager mLayoutManager;
    private CardPlayListAdapter mAdapter;
    private ArrayList<CardPlayListItem> myDataset;

    private CustomSpinnerAdapter mTypeAdapter,mBonusAdapter,mCompetitionAdpater,mEmissionAdapter,mJeuAdapter,mCommentateurAdapter;
    private ArrayList spinnerValues;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getArgumentFromActivity();
        initDataSet();

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new CardPlayListAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
//        mAdapter.SetOnItemClickListener(new CardPlayListAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                CardPlayListItem mCardPlayListItem = mAdapter.getItem(position);
//                Toast.makeText(getActivity(), mCardPlayListItem.getTitle(), Toast.LENGTH_LONG).show();
//            }
//        });

    }
    private  void initDataSet(){
        myDataset = new ArrayList<CardPlayListItem>();
        CardPlayListItem myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_playlist);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_playlist);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_playlist);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_logo_ogaming);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_logo_ogaming);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_logo_ogaming);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_logo_ogaming);
        myDataset.add(myCardPlayListItem);
        myCardPlayListItem = new CardPlayListItem("EEEEEEEEA E JE EJE JE EJE JEJEJ EJEJEE JEJJ EJEE EJ","il y 3 mois", "OGamingTv","20",R.drawable.ic_logo_ogaming);
        myDataset.add(myCardPlayListItem);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycleview, container, false);
        ButterKnife.inject(this, view);
        return view;
    }
    public void getArgumentFromActivity() {
        args = getArguments();
        if (args != null) {
            mTri = args.getString("ACTU");
        } else {
            mTri = "lol";
        }
    }

    public void initSpinnerList(){
        //ArrayList<String>
    }
    public void initSpinner(){
      //  mTypeAdapter = new CustomSpinnerAdapter(getActivity(), R.layout.custom_spinner, spinnerValues);
    }
}
