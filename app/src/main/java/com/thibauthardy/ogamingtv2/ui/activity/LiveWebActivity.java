package com.thibauthardy.ogamingtv2.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.thibauthardy.ogamingtv2.R;

import butterknife.ButterKnife;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class LiveWebActivity extends Activity {


    private WebView mWebView;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
        ButterKnife.inject(this);
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras != null) {
                mWebView = (WebView) findViewById(R.id.wv_live_activity);
                mWebView.getSettings().setJavaScriptEnabled(true);
                mWebView.loadUrl(extras.getString("URL_TWITCH"));

            }
        }
    }
}