package com.thibauthardy.ogamingtv2.models.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.ui.fragments.VodsFragment;
/**
 * Created by Thibault HARDY on 02/12/2014.
 */

public class TabPageAdapter extends FragmentPagerAdapter {

    private Bundle args;
    private String youtubeChannel;
    private VodsFragment fragment;
    // Tab titles
    private String[] tabs = { "OgamingTv", "Chips et Noi", "Ponf et Thud" };

    public TabPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
            args = new Bundle();
        switch (index) {
            case 0:
                Log.i("tabPage","og tv");
                args.putString("URI","Ogamingtv");
                args.putInt("COLOR",R.color.OGBlueFondSite);
                fragment = new VodsFragment() ;
                fragment.setArguments(args);
                return fragment;
            case 1:
                Log.i("tabPage","C et N");
                args.putString("URI","Chips et Noi");
                args.putInt("COLOR",R.color.OGlightblue);
                fragment = new VodsFragment() ;
                fragment.setArguments(args);
                return fragment;
            case 2:
                Log.i("tabPage","P et T");
                args.putString("URI","Ponf et Thud");
                args.putInt("COLOR",R.color.OGOrange);
                fragment = new VodsFragment() ;
                fragment.setArguments(args);
                return fragment;
        }
        return null;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }

}