package com.thibauthardy.ogamingtv2.models.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;

import java.util.ArrayList;

/**
 * Created by Thibault HARDY on 28/01/2015.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    ArrayList<String> strings;
    Context mCtx;

    public CustomSpinnerAdapter(Context ctx, int txtViewResourceId, ArrayList<String> objects) {
        super(ctx, txtViewResourceId, objects);

        strings = objects;
    }
    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }
    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =  (LayoutInflater) mCtx.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View mySpinner = inflater.inflate(R.layout.custom_spinner, parent,false);

        TextView main_text = (TextView) mySpinner .findViewById(R.id.tv_custom_spinner);
        main_text.setText(strings.get(position));
        return mySpinner;
    }
}