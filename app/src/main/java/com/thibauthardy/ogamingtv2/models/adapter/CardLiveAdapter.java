package com.thibauthardy.ogamingtv2.models.adapter;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardLiveItem;

import java.util.ArrayList;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class CardLiveAdapter extends RecyclerView.Adapter<CardLiveAdapter.MyViewHolder> {

    private ArrayList<CardLiveItem> mCardLiveDataSet;
    private String mPackageName;
    OnItemClickListener mItemClickListener;

    public CardLiveAdapter(ArrayList<CardLiveItem> vod, String packageName) {
        this.mCardLiveDataSet = vod;
        this.mPackageName = packageName;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView textView;
        VideoView mVideoView;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.imageView = (ImageView) itemView.findViewById(R.id.iv_card_stream);
            this.textView = (TextView) itemView.findViewById(R.id.tv_card_stream_live);
            this.mVideoView = (VideoView) itemView.findViewById(R.id.card_stream_video_player);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_stream, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int listPosition) {

        ImageView imageView = myViewHolder.imageView;
        TextView textView = myViewHolder.textView;
        VideoView mVideoView = myViewHolder.mVideoView;
        imageView.setImageResource(mCardLiveDataSet.get(listPosition).getImageLive());
        Log.i("player()", "i play now");

        mVideoView.setVideoURI(Uri.parse("android.resource://" + mPackageName + "/" + mCardLiveDataSet.get(listPosition).getUrlVideo()));
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        mVideoView.start();
        if(!mVideoView.isPlaying()){
            mVideoView.start();
        }
    }

    @Override
    public int getItemCount() {
        return mCardLiveDataSet.size();
    }

    public CardLiveItem getItem(int Position) {
        return mCardLiveDataSet.get(Position);
    }
}
