package com.thibauthardy.ogamingtv2.models.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardActuItem;

import java.util.ArrayList;

/**
 * Created by Thibault HARDY on 09/12/2014.
 */
public class CardActuAdapter extends RecyclerView.Adapter<CardActuAdapter.MyViewHolder> {


    private ArrayList<CardActuItem> cardActuDataSet;
    OnItemClickListener mItemClickListener;

    public CardActuAdapter(ArrayList<CardActuItem> actu){
        this.cardActuDataSet = actu;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageViewActu;
        TextView textViewDate;
        TextView textViewAuthor;
        TextView textViewNbComments;
        TextView textViewChannel;
        TextView textViewTitle;
        TextView textViewContents;


            public MyViewHolder(View itemView) {
                super(itemView);

                this.imageViewActu = (ImageView) itemView.findViewById(R.id.iv_card_actu_2);
                this.textViewDate = (TextView) itemView.findViewById(R.id.tv_card_actu_date_2);
                this.textViewAuthor = (TextView) itemView.findViewById(R.id.tv_card_actu_author);
                this.textViewNbComments = (TextView) itemView.findViewById(R.id.tv_card_actu_nb_comments);
                this.textViewChannel = (TextView) itemView.findViewById(R.id.tv_card_actu_channel);
                this.textViewTitle = (TextView) itemView.findViewById(R.id.tv_card_actu_title);
                this.textViewContents = (TextView) itemView.findViewById(R.id.tv_card_actu_contenu);
                itemView.setOnClickListener(this);
            }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
    public interface OnItemClickListener {
        public void onItemClick(View view , int position);

    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_actu_2, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int listPosition) {

        ImageView imageViewActu = myViewHolder.imageViewActu;
        TextView textViewDate = myViewHolder.textViewDate;
        TextView textViewAuthor = myViewHolder.textViewAuthor;
        TextView textViewNbComments  = myViewHolder.textViewNbComments;
        TextView textViewChannel = myViewHolder.textViewChannel;
        TextView textViewTitle = myViewHolder.textViewTitle;
        TextView textViewContents = myViewHolder.textViewContents;


        imageViewActu.setImageResource(cardActuDataSet.get(listPosition).getImageActu());
        textViewDate.setText(cardActuDataSet.get(listPosition).getDate());
        textViewAuthor.setText(cardActuDataSet.get(listPosition).getAuthor());
        textViewNbComments.setText(cardActuDataSet.get(listPosition).getNbComments());
        textViewChannel.setText(cardActuDataSet.get(listPosition).getChannel());
        textViewTitle.setText(verifSizeString(cardActuDataSet.get(listPosition).getTitle(),51));
        textViewContents.setText(verifSizeString(cardActuDataSet.get(listPosition).getContents(),180));


    }

    public String verifSizeString(String stringToTest, int lenght){

        char[] cutString = new char[lenght];
        StringBuilder mStringTest = new StringBuilder();
        if(stringToTest.length() >  lenght){

            stringToTest.getChars(0,lenght-1,cutString,0);
            for (char c : cutString){
                mStringTest.append(c);
            }
            mStringTest.append("...");

            return mStringTest.toString();
        }else{
           return stringToTest;
        }
    }

    @Override
    public int getItemCount() {
        return cardActuDataSet.size();
    }

    public CardActuItem getItem(int Position){
        return cardActuDataSet.get(Position);
    }
}
