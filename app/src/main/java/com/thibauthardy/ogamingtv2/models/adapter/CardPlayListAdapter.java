package com.thibauthardy.ogamingtv2.models.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardPlayListItem;

import java.util.ArrayList;

/**
 * Created by Thibault HARDY on 10/12/2014.
 */
public class CardPlayListAdapter extends RecyclerView.Adapter<CardPlayListAdapter.MyViewHolder>  {

    private int nbCharsMax = 51;

    private ArrayList<CardPlayListItem> cardPlayListDataSet;
    OnItemClickListener mItemClickListener;

    public CardPlayListAdapter(ArrayList<CardPlayListItem> actu){
        this.cardPlayListDataSet = actu;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewNbVideo;
        TextView textViewTitle;
        TextView textViewOwner;
        TextView textViewDate;
        ImageView imageViewPlaylist;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewTitle = (TextView) itemView.findViewById(R.id.tv_card_playlist_title);
            this.textViewNbVideo = (TextView) itemView.findViewById(R.id.tv_card_playlist_nb_video);
            this.textViewOwner = (TextView) itemView.findViewById(R.id.tv_card_playlist_owner);
            this.textViewDate = (TextView) itemView.findViewById(R.id.tv_card_playlist_date);
            this.imageViewPlaylist = (ImageView) itemView.findViewById(R.id.iv_card_playlist);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
    public interface OnItemClickListener {
        public void onItemClick(View view , int position);

    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_playlist, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int listPosition) {
        TextView textViewTitle = myViewHolder.textViewTitle;
        TextView textViewDate = myViewHolder.textViewDate;
        TextView textViewOwner = myViewHolder.textViewOwner;
        TextView textViewNbVideo = myViewHolder.textViewNbVideo;
        ImageView imageViewPlaylist = myViewHolder.imageViewPlaylist;

        textViewNbVideo.setText(cardPlayListDataSet.get(listPosition).getNbVideo());
        textViewOwner.setText(cardPlayListDataSet.get(listPosition).getOwner());
        //Gestion de la longeur du titre
        String test = cardPlayListDataSet.get(listPosition).getTitle();
        char[] cutString = new char[nbCharsMax];
        StringBuilder mStringTest = new StringBuilder();
        if(test.length() >  nbCharsMax){
            test.getChars(0,nbCharsMax-1,cutString,0);
            for (char c : cutString){
                mStringTest.append(c);
            }
            mStringTest.append("...");
            textViewTitle.setText(mStringTest);
        }else{
            textViewTitle.setText(cardPlayListDataSet.get(listPosition).getTitle());
        }
        textViewOwner.setText(cardPlayListDataSet.get(listPosition).getOwner());
        textViewDate.setText("" + test.length());
        imageViewPlaylist.setImageResource(cardPlayListDataSet.get(listPosition).getImagePlaylist());

    }

    @Override
    public int getItemCount() {
        return cardPlayListDataSet.size();
    }

    public CardPlayListItem getItem(int Position){
        return cardPlayListDataSet.get(Position);
    }
}