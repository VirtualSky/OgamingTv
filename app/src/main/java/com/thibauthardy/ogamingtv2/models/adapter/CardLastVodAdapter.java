package com.thibauthardy.ogamingtv2.models.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.dao.CardLastVodItem;

import java.util.ArrayList;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class CardLastVodAdapter extends RecyclerView.Adapter<CardLastVodAdapter.MyViewHolder> {

    private ArrayList<CardLastVodItem> mCardLastVodDataSet;
    OnItemClickListener mItemClickListener;

    public CardLastVodAdapter(ArrayList<CardLastVodItem> vod){
        this.mCardLastVodDataSet = vod;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        TextView textViewDate;
        TextView textViewGameName;
        TextView textViewNbComments;
        ImageView imageViewVod;
        TextView textViewTitle;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.textViewDate = (TextView) itemView.findViewById(R.id.tv_card_last_vod_date);
            this.textViewNbComments = (TextView) itemView.findViewById(R.id.tv_card_last_vod_nb_comments);
            this. textViewGameName = (TextView) itemView.findViewById(R.id.tv_card_last_vod_channel_name);
            this.imageViewVod = (ImageView) itemView.findViewById(R.id.iv_card_last_vod);
            this.textViewTitle = (TextView) itemView.findViewById(R.id.tv_card_last_vod_title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
    public interface OnItemClickListener {
        public void onItemClick(View view , int position);

    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_last_vod, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int listPosition) {

        TextView textViewDate = myViewHolder.textViewDate;
        TextView textViewNbComments = myViewHolder.textViewNbComments;
        TextView textViewGameName = myViewHolder. textViewGameName;
        ImageView imageViewVod = myViewHolder.imageViewVod;
        TextView textViewTitle = myViewHolder.textViewTitle;


        /*set data*/
        textViewDate.setText(mCardLastVodDataSet.get(listPosition).getDate());
        textViewNbComments.setText(mCardLastVodDataSet.get(listPosition).getNbComments());
        textViewGameName.setText(mCardLastVodDataSet.get(listPosition).getGameName());
        imageViewVod.setImageResource(mCardLastVodDataSet.get(listPosition).getImageVod());

        //TODO if getContenu > a N caract do ...
        String test = (mCardLastVodDataSet.get(listPosition).getTitle());
        //textViewDate.setText("" + test.length());
        int nbCharsMax = 110;
        char[] cutString = new char[nbCharsMax];
        StringBuilder mStringTest = new StringBuilder();
        if(test.length() > nbCharsMax){
            test.getChars(0, nbCharsMax -1,cutString,0);
            for (char c : cutString){
                mStringTest.append(c);
            }
            mStringTest.append("...");
            textViewTitle.setText(mStringTest);
        }else{
            textViewTitle.setText((mCardLastVodDataSet.get(listPosition).getTitle()));
        }
    }


    @Override
    public int getItemCount() {
        return mCardLastVodDataSet.size();
    }

    public CardLastVodItem getItem(int Position){
        return mCardLastVodDataSet.get(Position);
    }
}
