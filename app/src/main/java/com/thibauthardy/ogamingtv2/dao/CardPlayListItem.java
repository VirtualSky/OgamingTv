package com.thibauthardy.ogamingtv2.dao;

/**
 * Created by Thibault HARDY on 10/12/2014.
 */
public class CardPlayListItem {

    private String mTitle;
    private String mDate;
    private String mOwner;
    private String mNbVideo;
    private int mImagePlaylist;

    public CardPlayListItem(String Title, String Date, String Owner,String NbVideo,int ImagePlaylist){

        this.mTitle = Title;
        this.mDate = Date;
        this.mOwner = Owner;
        this.mNbVideo = NbVideo;
        this.mImagePlaylist = ImagePlaylist;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getOwner() {
        return mOwner;
    }

    public void setOwner(String mOwner) {
        this.mOwner = mOwner;
    }

    public int getImagePlaylist() {
        return mImagePlaylist;
    }

    public void setImagePlaylist(int mImagePlaylist) {
        this.mImagePlaylist = mImagePlaylist;
    }

    public String getNbVideo() {
        return mNbVideo;
    }

    public void setNbVideo(String mNbVideo) {
        this.mNbVideo = mNbVideo;
    }
}
