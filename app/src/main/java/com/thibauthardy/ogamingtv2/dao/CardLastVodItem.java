package com.thibauthardy.ogamingtv2.dao;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class CardLastVodItem {


    private String mDate;
    private String mGameName;
    private String mNbComments;
    private int mImageVod;
    private String mTitle;


    public CardLastVodItem(String Date, String NbComments, String GameName,int ImageVod, String Title){
        this.mTitle = Title;
        this.mDate = Date;
        this.mGameName = GameName;
        this.mNbComments = NbComments;
        this.mImageVod = ImageVod;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getGameName() {
        return mGameName;
    }

    public void setGameName(String mOwner) {
        this.mGameName = mOwner;
    }

    public int getImageVod() {
        return mImageVod;
    }

    public void setImageVod(int mImagePlaylist) {
        this.mImageVod = mImagePlaylist;
    }

    public String getNbComments() {
        return mNbComments;
    }

    public void setNbComments(String mNbVideo) {
        this.mNbComments = mNbVideo;
    }
}
