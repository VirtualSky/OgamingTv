package com.thibauthardy.ogamingtv2.dao;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class CardLiveItem {

    private int mImageLive;
    private String mLinkTwitch;
    private int mUrlVideo;
    private String mUrlChat;

    public CardLiveItem(int Image, String LinkTwitch,int UrlVideo){
        this.mImageLive = Image;
        this.mLinkTwitch = LinkTwitch;
        this.mUrlVideo = UrlVideo;

    }

    public String getLinkTwitch() {
        return mLinkTwitch;
    }

    public void setLinkTwitch(String Link) {
        this.mLinkTwitch = Link;
    }

    public int getImageLive() {
        return mImageLive;
    }

    public void setImageLive(int ImageLive) {
        this.mImageLive = ImageLive;
    }

    public int getUrlVideo() {
        return mUrlVideo;
    }

    public void setUrlVideo(int UrlVideo) {
        this.mUrlVideo = UrlVideo;
    }

    public String getUrlChat() {
        return mUrlChat;
    }

    public void setUrlChat(String UrlChat) {
        this.mUrlChat = UrlChat;
    }

}
