package com.thibauthardy.ogamingtv2.dao;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.thibauthardy.ogamingtv2.tools.genelist.AbstractItem;

import java.io.Serializable;

/**
 * Created by Thibault HARDY on 02/12/2014.
 */
public class DrawerItem extends AbstractItem{

    private int mImage;
    private String mTitle;
    private boolean isLive;
    private int mImageLive;
    private Fragment mFragment;
    private Class mActivity;
    private boolean isFooter;
    private boolean isSeparator;

    public DrawerItem(int Image, String Title, int ImageLive, boolean Live, Fragment fragment,boolean Footer,boolean isSeparator){
        this.mImage = Image;
        this.mTitle = Title;
        this.isLive = Live;
        this.mImageLive = ImageLive;
        this.mFragment = fragment;
        this.isFooter = Footer;
        this.isSeparator = isSeparator;

    }
    public DrawerItem(int Image, String Title, int ImageLive, boolean Live, Class activity ,boolean Footer,boolean isSeparator){
        this.mImage = Image;
        this.mTitle = Title;
        this.isLive = Live;
        this.mImageLive = ImageLive;
        this.mActivity = activity;
        this.isFooter = Footer;
        this.isSeparator = isSeparator;

    }

    public int getImage() {
        return mImage;
    }

    public void setImage(int mImage) {
        this.mImage = mImage;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean isLive) {
        this.isLive = isLive;
    }

    public int getImageLive() {
        return mImageLive;
    }

    public void setImageLive(int mImageLive) {
        this.mImageLive = mImageLive;
    }

    public Fragment getFragment() {
        return mFragment;
    }

    public void setFragment(Fragment fragment) {
        this.mFragment = fragment;
    }

    public boolean isFooter() {
        return isFooter;
    }

    public void setFooter(boolean isFooter) {
        this.isFooter = isFooter;
    }

    public Class getActivity() {
        return mActivity;
    }

    public void setActivity(Class Activity) {
        this.mActivity = Activity;
    }

    public boolean isSeparator() {
        return isSeparator;
    }

    public void setSeparator(boolean isSeparator) {
        this.isSeparator = isSeparator;
    }
}