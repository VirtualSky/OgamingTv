package com.thibauthardy.ogamingtv2.dao;

import android.media.Image;

import com.thibauthardy.ogamingtv2.tools.genelist.AbstractItem;

/**
 * Created by Thibault HARDY on 05/12/2014.
 */
public class ListVodItem extends AbstractItem {

    private int mImage;
    private String mName;
    private String mOwner;
    private String mNumberView;
    private String mUrl;


    public ListVodItem(int Image, String Name, String Owner, String NumberView, String Url){
        this.mImage  = Image;
        this.mName = Name;
        this.mNumberView = NumberView;
        this.mOwner = Owner;
        this.mUrl = Url;
    }


    public int getImage() {
        return mImage;
    }

    public void setImage(int mImage) {
        this.mImage = mImage;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getOwner() {
        return mOwner;
    }

    public void setOwner(String mOwner) {
        this.mOwner = mOwner;
    }

    public String getNumberView() {
        return mNumberView;
    }

    public void setNumberView(String mNumberView) {
        this.mNumberView = mNumberView;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }
}
