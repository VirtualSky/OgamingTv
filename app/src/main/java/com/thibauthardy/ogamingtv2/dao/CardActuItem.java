package com.thibauthardy.ogamingtv2.dao;

/**
 * Created by Thibault HARDY on 09/12/2014.
 */
public class CardActuItem {

    private int mImageActu;
    private String mDate;
    private String mAuthor;
    private String mNbComments;
    private String mChannel;
    private String mTitle;
    private String mContents;
    private String mLink;


    public CardActuItem(int Image, String date,String Author,String NbComments,String Channel,String title, String Contents,String Link){

        this.mImageActu = Image;
        this.mDate = date;
        this.mAuthor = Author;
        this.mNbComments = NbComments;
        this.mChannel = Channel;
        this.mTitle = title;
        this.mContents = Contents;

        this.mLink = Link;
    }
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String Title) {
        this.mTitle = Title;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String Date) {
        this.mDate = Date;
    }

    public String getContents() {
        return mContents;
    }

    public void setContents(String Contenu) {
        this.mContents = Contenu;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String Link) {
        this.mLink = Link;
    }

    public int getImageActu() {
        return mImageActu;
    }

    public void setImageActu(int ImageActu) {
        this.mImageActu = ImageActu;
    }

    public String getChannel() {
        return mChannel;
    }

    public void setChannel(String Channel) {
        this.mChannel = Channel;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String Author) {
        this.mAuthor = Author;
    }

    public String getNbComments() {
        return mNbComments;
    }

    public void setNbComments(String NbComments) {
        this.mNbComments = NbComments;
    }
}
