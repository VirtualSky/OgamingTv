package com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu;

import com.thibauthardy.ogamingtv2.R;

/**
 * Created by Thibault HARDY on 28/11/2014.
 */
public enum DrawerMenuVideoItem implements DrawerSubMenuItem {

    OGAMING(R.drawable.ic_logo_ogaming,R.string.video_ogaming),
    PONFTHUD(R.drawable.ic_ponf_thud,R.string.video_ponf_thud),
    CHIPSNOI(R.drawable.ic_chips_noi,R.string.video_chips_noi);


    private int mDrawableRessourceId;
    private int mNameRessourceId;

    private DrawerMenuVideoItem(int drawableRessourceId, int nameRessourceId){
        this.mDrawableRessourceId = drawableRessourceId;
        this.mNameRessourceId = nameRessourceId;
    }

    @Override
    public int getNameRessourceId() {
        return mNameRessourceId;
    }
    @Override
    public int getDrawableRessourceId() {
        return mDrawableRessourceId;
    }
    @Override
    public int getSize(){
        return  DrawerMenuTvItem.values().length;
    }
}

