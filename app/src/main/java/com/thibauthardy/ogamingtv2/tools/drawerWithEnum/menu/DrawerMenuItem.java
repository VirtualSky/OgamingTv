package com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuTvItem;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerSubMenuItem;

/**
 * Created by Thibault HARDY on 26/11/2014.
 */
public enum DrawerMenuItem {

            MENU_ACTU(R.drawable.ic_logo_ogaming,R.string.MenuActu, DrawerMenuNewItem.values()),
            MENU_WEBTV(R.drawable.ic_logo_ogaming,R.string.MenuWebTv, DrawerMenuTvItem.values()),
            MENU_AGENDA(R.drawable.ic_logo_ogaming,R.string.MenuAgenda,null),
            MENU_VIDEO(R.drawable.ic_logo_ogaming,R.string.MenuVideo,DrawerMenuVideoItem.values()),
            MENU_SHOP(R.drawable.ic_logo_ogaming,R.string.MenuShop,null),
            MENU_FORUM(R.drawable.ic_logo_ogaming,R.string.MenuForum,null);

    private DrawerMenuItem(int drawableRessourceId, int nameRessourceId, DrawerSubMenuItem drawerSubMenuItem[]){
        this.mDrawableRessourceId = drawableRessourceId;
        this.mNameRessourceId = nameRessourceId;
        this.mSubItem = drawerSubMenuItem;

    }

    private DrawerSubMenuItem mSubItem [];
    private int mDrawableRessourceId;
    private int mNameRessourceId;


    public int getNameRessourceId() {
        return mNameRessourceId;
    }

    public int getDrawableRessourceId() {
        return mDrawableRessourceId;
    }

    public DrawerSubMenuItem[] getSubItem() {
        return mSubItem;
    }


}
