package com.thibauthardy.ogamingtv2.tools.drawerWithEnum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuItem;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerSubMenuItem;

/**
 * Created by Thibault HARDY on 26/11/2014.
 */
public class MenuExpandableAdapter extends BaseExpandableListAdapter {


    private final LayoutInflater mLayoutInflater;
    private final Context mContext;

    public MenuExpandableAdapter(Context context){
        this.mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }


    @Override
    public int getGroupCount() {
        return DrawerMenuItem.values().length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
       DrawerSubMenuItem[] subItem = DrawerMenuItem.values()[groupPosition].getSubItem();
        if (subItem == null){
            return 0;
        }
        return subItem.length;
    }

    @Override
    public DrawerMenuItem getGroup(int groupPosition) {
        return DrawerMenuItem.values()[groupPosition];
    }

    @Override
    public DrawerSubMenuItem getChild(int groupPosition, int childPosition) {
        DrawerSubMenuItem[] subItem = DrawerMenuItem.values()[groupPosition].getSubItem();
        if (subItem == null){
            return null;
        }
        return subItem[childPosition];
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        DrawerMenuItem item = getGroup(groupPosition);

        if(convertView == null){
            convertView =  mLayoutInflater.inflate(R.layout.drawer_ex_list_item, parent,false);
        }
        ImageView mImage = (ImageView) convertView.findViewById(R.id.iv_drawer_image_item);
        TextView mTitle = (TextView) convertView.findViewById(R.id.tv_drawer_ex_list_item);
        mImage.setImageResource(item.getDrawableRessourceId());
        mTitle.setText(item.getNameRessourceId());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        DrawerSubMenuItem subItem = getChild(groupPosition,childPosition);

        if(convertView == null){
            convertView =  mLayoutInflater.inflate(R.layout.drawer_ex_list_child_item, parent,false);
        }

        ImageView mImage = (ImageView) convertView.findViewById(R.id.iv_drawer_image_child_item);
        TextView mTitle = (TextView) convertView.findViewById(R.id.tv_drawer_list_child_item);

        mImage.setImageResource(subItem.getDrawableRessourceId());
        mTitle.setText(subItem.getNameRessourceId());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
