package com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu;

import com.thibauthardy.ogamingtv2.R;

/**
 * Created by Thibault HARDY on 26/11/2014.
 */
public enum DrawerMenuTvItem implements DrawerSubMenuItem {

    LOL(R.drawable.ic_logo_ogaming,R.string.tv_lol),
    STARCRAFT(R.drawable.ic_logo_ogaming,R.string.tv_starcraft),
    ANNOS(R.drawable.ic_logo_ogaming,R.string.tv_annos),
    HEARTHSTONE(R.drawable.ic_logo_ogaming,R.string.tv_hearthstone),
    LORGARD(R.drawable.ic_logo_ogaming,R.string.tv_lorgard),
    SILVERMEDUSA(R.drawable.ic_logo_ogaming,R.string.tv_silvermedusa),
    GENERALIST(R.drawable.ic_logo_ogaming,R.string.tv_generalist);



    private int mDrawableRessourceId;
    private int mNameRessourceId;

    private DrawerMenuTvItem(int drawableRessourceId, int nameRessourceId){
        this.mDrawableRessourceId = drawableRessourceId;
        this.mNameRessourceId = nameRessourceId;
    }

    @Override
    public int getNameRessourceId() {
        return mNameRessourceId;
    }
    @Override
    public int getDrawableRessourceId() {
        return mDrawableRessourceId;
    }
    @Override
    public int getSize(){
     return  DrawerMenuTvItem.values().length;
    }
}
