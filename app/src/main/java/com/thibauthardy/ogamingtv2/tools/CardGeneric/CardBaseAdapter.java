package com.thibauthardy.ogamingtv2.tools.CardGeneric;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Thibault HARDY on 11/12/2014.
 */
public class CardBaseAdapter<T extends ArrayList<AbstractCardItem>> extends RecyclerView.Adapter<CardBaseAdapter<T>.MyViewHolder> {

    private ArrayList<AbstractCardItem> mData;
    private int mIdLayout;
    OnItemClickListener mItemClickListener;

    public CardBaseAdapter(T data,int IdLayout){
        this.mData = data;
        this.mIdLayout =IdLayout;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //initialise yours elements

        public MyViewHolder(View itemView) {
            super(itemView);
            //link your elements
            // this.element = (Type element) item.view.findViewById(R.id.element);

            itemView.setOnClickListener(this);
        }
        /**
         * manage clic on card item
         * */
        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
    public interface OnItemClickListener {
        public void onItemClick(View view , int position);

    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mIdLayout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        //initialise and set your elements
        //TypeElement element = myViewHolder.element
        // element.set....;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
    public AbstractCardItem getItem(int Position){
        return mData.get(Position);
    }

    /*Tools*/
    public String verifSizeString(String stringToTest){
        int nbCharsMax = 51;
        char[] cutString = new char[nbCharsMax];
        StringBuilder mStringTest = new StringBuilder();
        if(stringToTest.length() >  nbCharsMax){

            stringToTest.getChars(0,nbCharsMax-1,cutString,0);
            for (char c : cutString){
                mStringTest.append(c);
            }
            mStringTest.append("...");

            return mStringTest.toString();
        }else{
            return stringToTest;
        }
    }
}
