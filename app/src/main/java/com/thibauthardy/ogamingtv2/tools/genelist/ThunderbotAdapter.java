package com.thibauthardy.ogamingtv2.tools.genelist;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public class ThunderbotAdapter<T extends List<AbstractItem>> extends BaseAdapter{
	Context mContext;
	private T mData;
	private Class<?> mClazz;

	 public ThunderbotAdapter(Context ctx, T data, Class<? extends AbstractCellView<AbstractItem>> clazz) {
		super();
		this.mContext = ctx;
		mData = data;
		mClazz = clazz;
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public AbstractItem getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View recycleView, ViewGroup parent) {
		AbstractCellView<AbstractItem> newView = null;
		if (recycleView == null) {
			try {
				newView = (AbstractCellView<AbstractItem>) mClazz.getConstructor(Context.class).newInstance(mContext);
			} catch (Exception e) {
				Log.e(this.getClass().getSimpleName(), "Exception !", e);
			}
			newView.setData(getItem(position));
			return newView;
		} else {
			AbstractCellView<AbstractItem> convertedView = (AbstractCellView<AbstractItem>) recycleView;
			convertedView.setData(getItem(position));
			return convertedView;
		}
	}

}
