package com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu;

import com.thibauthardy.ogamingtv2.R;

/**
 * Created by Thibault HARDY on 28/11/2014.
 */
public enum DrawerMenuNewItem implements DrawerSubMenuItem {

    All(R.drawable.ic_logo_ogaming,R.string.new_All),
    LOL(R.drawable.ic_logo_ogaming,R.string.new_lol),
    STARCRAFT(R.drawable.ic_starcraft_logo,R.string.new_starcraft),
    DOC(R.drawable.ic_doc,R.string.new_doc),
    HEARTHSTONE(R.drawable.ic_hearthstone,R.string.new_hearthstone),
    HOTS(R.drawable.ic_hots,R.string.new_hots),
    DOTA(R.drawable.ic_dota,R.string.new_dota),
    FPS(R.drawable.ic_divers,R.string.new_fps),
    VF(R.drawable.ic_versusf,R.string.new_versusF),
    DIVERS(R.drawable.ic_divers,R.string.new_divers);


    private int mDrawableRessourceId;
    private int mNameRessourceId;

    private DrawerMenuNewItem(int drawableRessourceId, int nameRessourceId){
        this.mDrawableRessourceId = drawableRessourceId;
        this.mNameRessourceId = nameRessourceId;
    }

    @Override
    public int getNameRessourceId() {
        return mNameRessourceId;
    }
    @Override
    public int getDrawableRessourceId() {
        return mDrawableRessourceId;
    }
    @Override
    public int getSize(){
        return  DrawerMenuTvItem.values().length;
    }
}
