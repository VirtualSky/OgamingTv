package com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu;

/**
 * Created by Thibault HARDY on 26/11/2014.
 */
public interface DrawerSubMenuItem {

    public int getDrawableRessourceId();

    public int getNameRessourceId();

    public int getSize();
}