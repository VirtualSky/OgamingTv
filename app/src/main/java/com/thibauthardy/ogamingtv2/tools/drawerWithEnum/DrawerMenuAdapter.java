package com.thibauthardy.ogamingtv2.tools.drawerWithEnum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thibauthardy.ogamingtv2.R;
import com.thibauthardy.ogamingtv2.tools.drawerWithEnum.menu.DrawerMenuItem;

/**
 * Created by Thibault HARDY on 26/11/2014.
 */
public class DrawerMenuAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public DrawerMenuAdapter(Context context){
        this.mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return DrawerMenuItem.values().length;
    }

    @Override
    public DrawerMenuItem getItem(int position) {
        return DrawerMenuItem.values()[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerMenuItem item = getItem(position);

        if(convertView == null){
            convertView =  mLayoutInflater.inflate(R.layout.drawer_ex_list_item, parent,false);
        }
      ImageView  mImage = (ImageView) convertView.findViewById(R.id.iv_drawer_image_item);
      TextView  mTitle = (TextView) convertView.findViewById(R.id.tv_drawer_ex_list_item);
      mImage.setImageResource(item.getDrawableRessourceId());
      mTitle.setText(item.getNameRessourceId());
        return convertView;
    }



}
