package com.thibauthardy.ogamingtv2.tools.genelist;

import android.content.Context;
import android.widget.LinearLayout;

public abstract class AbstractCellView<T extends AbstractItem> extends LinearLayout {

	public AbstractCellView(Context context) {
		super(context);
	}

	public abstract void setData(T t);
	
}
