package com.thibauthardy.ogamingtv2.utils;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * This class permits to save data and get it from SharedPreferences.
 *
 * @author Santoine
 */
public class PreferenceManager {

    /**
     * The preferences.
     */
    private SharedPreferences preferences;

    /**
     * Instantiates a new preferences manager.
     *
     * @param context the context
     */
    public PreferenceManager(Context context) {
        this.preferences = context.getSharedPreferences("ApplicationPreferences", Context.MODE_PRIVATE);
    }

    /**
     * Put data.
     *
     * @param key   the key
     * @param value the value
     */
    public void putDataString(String key, String value) {
        preferences.edit().putString(key, value).commit();
    }

    public void putDataInt(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }

    public void putDataLong(String key, long value) {
        preferences.edit().putLong(key, value).commit();
    }
    public void putDataBoolean(String key,boolean value){
        preferences.edit().putBoolean(key,value).commit();
    }

    /**
     * Get data.
     *
     * @param key
     * @return the value
     */
    public String getDataString(String key) {
        return preferences.getString(key, null);
    }

    public int getDataInt(String key) {
        return preferences.getInt(key, 0);
    }

    public long getDataLong(String key) {
        return preferences.getLong(key, 0);
    }
    /**
     * default value false
     * */
    public boolean getDataBoolean(String key){
        return preferences.getBoolean(key,false);
    }
}
